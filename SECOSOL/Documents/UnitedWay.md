# Becoming a United Way NCA Partner

On November 18, 2024, I received an email reply from webpledge@uwnca.org
to my inquiry about becoming a United Way partner that said:

> Good afternoon, Mr. Elkner -
>
> United Way NCA has an annual process for accepting applications from area
> nonprofits. Please send an email directly to:  Partner Engagement - United
> Way NCA <partner@uwnca.org> expressing your interest and that team will be
> able to explain the requirements and the process.
>
> You may also find this link a good starting place to learn more.
> https://unitedwaynca.org/for-partner-nonprofits/partner-guide/
>
> We appreciate the support you are showing for the community.
> 
> Tony Paul
> United Way NCA Helpdesk
> 202.488.2006 | webpledge@uwnca.org

The [Partner Quickstart
Guide](https://unitedwaynca.org/for-partner-nonprofits/partner-guide/) contains
the information we need. SECOSOL won't be eligible until it reaches the $50K
per year revenue threshold.
