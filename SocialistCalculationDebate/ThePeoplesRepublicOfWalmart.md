# The People's Republic of Walmart

by [Leigh Phillips](https://www.leighphillips.work/) and
[Michal Rozworski](http://rozworski.org/).

## Chapters

* Introduction
* Could Walmart Be a Secret Socialist Plot?
* Islands of Tyranny
* Mapping the Amazon
* Index Funds as Sleeper Agents of Planning
* Nationalization Is Not Enough
* Did They Even PLan the Soviet Union?
* Hardly Automated Space Communism
* Allende's Socialist Internt
* Planning the Good Anthropocene
* Conclusion: Planning Works

The central thesis of this book is stated at the beginning of the conclusion:

> Planning exists all around us, and it clearly works; otherwise capitalists
> would not make such comprehensive use of it. That’s the simple message of
> this book and one that strikes at the heart of the dogma that “there is no
> alternative.” 

and at the start of the following paragraph:

> However, if the good news is that planning works, the bad news is precisely
> that it currently works within the confines of a profit system that restricts
> what is able to be produced to that which is profitable; and so long as this
> is profitable, the system allows what is harmful to continue to be produced.


## Could Walmart Be a Secret Socialist Plot?

* Austrian School economist Ludwig von Mises makes what is perhaps the
  strongest argument ever mounted against socialism in his 1920 essay,
  *Economic Calculation in the Socialist Commonwealth*, stating that
  socialist planning is not theoretically possible, that no human process
  could possibly gather all the necessary data, assess it in real time, and
  produce plans that accurately describe supply and demand across all sectors,
  and that inefficiencies in the attempt to plan would result in social and
  economic barbarities - shortages, starvation, frustration and chaos.
  (pp. 27-29).
* "If something works in theory but not in practice, then there is usually
  something wrong with the theory. But it is equally true that if something in
  theory does not work, but in practice it does, then again, something must be
  wrong with the theory. And here is where the villainous Walmart enters our
  story.  Walmart is perhaps the best evidence we have that while planning
  appears not to work in Mises’s theory, it certainly does in practice. And
  then some." (p. 30).
* "Founder Sam Walton opened his first store, Wal-Mart Discount City, on
  July 2, 1962, in the non-city of Rogers, Arkansas, population 5,700."
  (p. 30).
* Walmart has been the largest company in the world by revenue since
  2014, with revenue in 2022 of over $572 billion. (Wikipedia List of
  largest companies by revenue).
* "[W]hile the company operates within the market, internally, as in any
  other firm, everything is planned. There is no internal market. The
  different departments, stores, trucks and suppliers do not compete against
  each other in a market; everything is coordinated." (p. 31).
* Walmart turned toward modern logistics long before many other large firms,
  and that it has been a trailblazer in logistics innovations that drive down
  costs.  (p. 32).

