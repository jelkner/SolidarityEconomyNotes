# The Strange, Wonderful History of How to Think Like a Computer Scientist

In 1999 Allen Downey released *How to Think Like a Computer Scientist*, an
introductory computer science text book, under the
[GNU Public License](https://www.gnu.org/licenses/gpl-3.0.html).  Later that
year, Jeff Elkner, a high school computer science teacher, adapted Allen's
original Java version of the text to use Python as the programming language.
In the preface to *Think Python: How to Think Like a Computer Scientist*, Allen
says that after receiving the remixed version, "I had the unusual
experience of learning Python by reading my own book." (Downey 2012, v).

This pioneering example of
[OER](https://en.wikipedia.org/wiki/Open_educational_resources), launched
three years before the term *open educational resources* was coined at UNESCO's
2002 Forum on Open Courseware (Wikipedia 2022), led to numerous remixes, both
into other programming languages and other natural languages, and it continues
to generate remixes twenty-four years later.

The majority of the remixes use Python as the programming language, and indeed
the use of the book grew along with the increasing popularity of Python as a
programming language for teaching introductory programming, allowing
*How to Think Like a Computer Scientist: Learning with Python* to play an
active part in the growth of Python in CS education.

One major remix in particular, Charles Severance's *Python for Everybody:
Exploring Data Using Python 3*, now provides educators with a complete video
series to accompany the book, and is available in serveral natural languages,
including Spanish, Italian, Portuguese, Polish, and German, with several other
translations in process. In the first paragraph of the preface, Charles says
that,

> It is quite natural for academics who are continuously told to “publish or
> perish” to want to always create something from scratch that is their own
> fresh creation.  This book is an experiment in not starting from scratch,
> but instead “remixing” the book titled Think Python: How to Think Like a
> Computer Scientist written by Allen B. Downey, Jeff Elkner, and others.
> (Severance 2016, iii).

This talk will share the early history of the project told by one of its
early participants. It will then describe some of the many remixes that
developed from it over the last quarter century, and reflect on what lessons
may be found in its history that are of use to OER practitioners today.


## References

* Downey, Allen. (2012). *Think Python: How to Think Like a Computer
  Scientist*. Sabastopol, CA: O'Reilly Media, Inc.
* Severance, Charles (2016). *Python for Everybody: Exploring Data Using
  Python 3*.
* Wikipedia contributors. (2022, December 2). Open educational resources.
  In Wikipedia, The Free Encyclopedia. Retrieved 14:38, December 22, 2022,
  from https://en.wikipedia.org/w/index.php?title=Open_educational_resources&oldid=1125137361


## Keywords

OER story
OER practice 
OER case study
Collaborative curriculum development
Python
Computer Science textbook


## Topics

* Theme 1: Celebrating the impact of Open Eucation and OER
* Theme 5: Wildcard. Creative practice in relation to openness


## Session Format

* 30 min Reflective practice / Research pres.


## Programming Language Remixes

### Python

* [How to Think Like a Computer Scientist: Learning with Python](https://www.greenteapress.com/thinkpython/thinkCSpy.pdf)
* [How to Think Like a Computer Scientist: Interactive Edition](https://runestone.academy/ns/books/published/thinkcspy/index.html)
* [How to Think Like a Computer Scientist: Learning with Python 3](https://buildmedia.readthedocs.org/media/pdf/howtothink/latest/howtothink.pdf)
* [Computer Programming for Everybody: Exploring Data Using Python 3](http://do1.dr-chuck.com/pythonlearn/EN_us/pythonlearn.pdf)

### C

* [How to Think Like a Computer Scientist: C Version](https://cs.indstate.edu/sternfl/pu/Think-C_v1.08.pdf)

### C++

* [How to think like a computer scientist: C++ Version, First Edition](https://www.inf.ufpr.br/cursos/ci067/Docs/thinkCScpp.pdf)
* [How to think like a computer scientist](https://www.overleaf.com/articles/how-to-think-like-a-computer-scientist-c-plus-plus-version/xrccfxvjfvxr.pdf)
* [ThinkCPP](https://github.com/AllenDowney/ThinkCPP)

### Logo

* [How to Think Like a Computer Scientist: C Version](https://openbookproject.net/thinkcs/archive/logo/english/thinklgo.pdf)


## Natural Language Remixes

### Spanish 

* [Python para todos: Explorando la información con Python 3](https://do1.dr-chuck.com/pythonlearn/ES_es/pythonlearn.pdf)

### Italian

* [Python para Todos: Explorando Dados com Python 3](https://do1.dr-chuck.com/pythonlearn/PT_br/pythonlearn.pdf)
