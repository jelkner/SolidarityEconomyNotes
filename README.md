# Solidarity Economy Notes

## Sections

* [Cooperatives and Socialism](CoopsAndSocialism)
* [Decidim](Decidim)
* [Design Justice](DesignJustice)
* [MayFirst Technology Movement](MayFirst)
* [Fight and build](FightAndBuild)

## Links

* [New Economy Coalition](https://neweconomy.net/solidarity-economy/)
* [The Center for Global Justice](https://www.globaljusticecenter.org/)
* [Grassroots Economic Organizing](https://geo.coop/)
