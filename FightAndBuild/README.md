# Fight and build: solidarity economy as ontological politics

Notes and reflections of the article [Fight and build: solidarity economy as
ontological politics](https://pmc.ncbi.nlm.nih.gov/articles/PMC9244216/pdf/11625_2022_Article_1165.pdf)
by Penn Loh and Boone W. Shear.

Opening sentence of abstract:

> This essay explores the potential of solidarity economy (SE) as theory,
> practice, and movement, to engender an ontological politics to create and
> sustain other worlds that can resolve the existential crises of ecological
> destruction and historic inequalities.
