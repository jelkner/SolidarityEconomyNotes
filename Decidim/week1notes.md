# Notes for Week 1: June 21 to 27, 2021

Thanks to the fine efforts of Louis Elkner-Alfaro of
[NOVA Web Development](https://novawebdevelopment.org/), we have two
instances of Decidim to experiment with, one for wild, open-ended
experimentation (decidim dogfood) and one for use organizing a real
community, [https://ustechworkercoops.org/](https://ustechworkercoops.org/).

Tomorrow I'll begin looking at the Decidim documentation site,
[Welcome to Decidim Documentation](https://docs.decidim.org/en/).

## Wednesday, June 23, 2021

By the time I arrived at the
[second page](https://docs.decidim.org/en/configure/) of the Decidim
documentation, it stated that, "Decidim is a Ruby on Rails library, and as such 
we try to follow their conventions."

Time to return to [The Odin Project](https://www.theodinproject.com/) and see
how quickly I can learn about Rails.
