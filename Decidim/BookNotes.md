# Decidim, a Technopolitical Network for Participatory Democracy

## Opening Quote

> Types of machines are easily matched with each type of society—not that
> machines are determining, but because they express those social forms
> capable of generating them and using them.
>
> Gilles Deleuze


## Important Quotes (with page numbers and comments)

* "We deeply believe that genuine democracy shall never be conquered without
   economic and social democracy, without equality and collective control over
   the productive and reproductive networks that shape our lives."  (page ix).
   Here is the foundational belief that underlies the Decidim project.

