# Decidim Installation Notes

Three years after our first foray into [Decidim](https://decidim.org/), we are
rebooting (along with [NOVA Web Development](https://novawebdevelopment.org/)
itself) our effort to learn to provide installation and training on the
platform to organizations interested in using technology in support of
democracy.


## Step 1: Installing ``rbenv``

I'm starting with an [Ubuntu 22.04](https://releases.ubuntu.com/jammy/) vm
hosted on [Linode](https://en.wikipedia.org/wiki/Linode).  I'll be following
the instructions on the [Manual installation
tutorial](https://docs.decidim.org/en/develop/install/manual.html) from
the Decidim website.

```
$ sudo apt install -y build-essential curl git libssl-dev zlib1g-dev
$ git clone https://github.com/rbenv/rbenv.git ~/.rbenv
$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
$ echo 'eval "$(rbenv init -)"' >> ~/.bashrc
$ source ~/.bashrc
$ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
$ rbenv install 3.1.1
```
Then several minutes transpired, during which I saw:
```
==> Downloading ruby-3.1.1.tar.gz...
-> curl -q -fL -o ruby-3.1.1.tar.gz https://cache.ruby-lang.org/pub/ruby/3.1/ruby-3.1.1.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 19.6M  100 19.6M    0     0  14.0M      0  0:00:01  0:00:01 --:--:-- 14.0M
==> Installing ruby-3.1.1...
-> ./configure "--prefix=$HOME/.rbenv/versions/3.1.1" --enable-shared --with-ext=openssl,psych,+
-> make -j 1
-> make install
==> Installed ruby-3.1.1 to /home/jelkner/.rbenv/versions/3.1.1

NOTE: to activate this Ruby version as the new default, run: rbenv global 3.1.1
```
So I did:
```
$ rbenv global 3.1.1
```


## Step 2: Installing [PostgreSQL](https://www.postgresql.org/) 
```
$ sudo apt install -y postgresql libpq-dev
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  libcommon-sense-perl libjson-perl libjson-xs-perl libllvm14 libpq5
  libtypes-serialiser-perl postgresql-14 postgresql-client-14
  postgresql-client-common postgresql-common ssl-cert
Suggested packages:
  postgresql-doc-14 postgresql-doc
The following NEW packages will be installed:
  libcommon-sense-perl libjson-perl libjson-xs-perl libllvm14 libpq-dev libpq5
  libtypes-serialiser-perl postgresql postgresql-14 postgresql-client-14
  postgresql-client-common postgresql-common ssl-cert
0 upgraded, 13 newly installed, 0 to remove and 0 not upgraded.
Need to get 42.1 MB of archives.
After this operation, 160 MB of additional disk space will be used.
Get:1 http://mirrors.linode.com/ubuntu jammy/main amd64 libcommon-sense-perl amd64 3.75-2build1 [21.1 kB]
Get:2 http://mirrors.linode.com/ubuntu jammy/main amd64 libjson-perl all 4.04000-1 [81.8 kB]
Get:3 http://mirrors.linode.com/ubuntu jammy/main amd64 libtypes-serialiser-perl all 1.01-1 [11.6 kB]
Get:4 http://mirrors.linode.com/ubuntu jammy/main amd64 libjson-xs-perl amd64 4.030-1build3 [87.2 kB]
Get:5 http://mirrors.linode.com/ubuntu jammy-updates/main amd64 libllvm14 amd64 1:14.0.0-1ubuntu1.1 [24.0 MB]
Get:6 http://mirrors.linode.com/ubuntu jammy-updates/main amd64 libpq5 amd64 14.11-0ubuntu0.22.04.1 [144 kB]
Get:7 http://mirrors.linode.com/ubuntu jammy-updates/main amd64 libpq-dev amd64 14.11-0ubuntu0.22.04.1 [147 kB]
Get:8 http://mirrors.linode.com/ubuntu jammy/main amd64 postgresql-client-common all 238 [29.6 kB]
Get:9 http://mirrors.linode.com/ubuntu jammy-updates/main amd64 postgresql-client-14 amd64 14.11-0ubuntu0.22.04.1 [1,222 kB]
Get:10 http://mirrors.linode.com/ubuntu jammy/main amd64 ssl-cert all 1.1.2 [17.4 kB]
Get:11 http://mirrors.linode.com/ubuntu jammy/main amd64 postgresql-common all 238 [169 kB]
Get:12 http://mirrors.linode.com/ubuntu jammy-updates/main amd64 postgresql-14 amd64 14.11-0ubuntu0.22.04.1 [16.2 MB]
Get:13 http://mirrors.linode.com/ubuntu jammy/main amd64 postgresql all 14+238 [3,288 B]
Fetched 42.1 MB in 1s (29.5 MB/s) 
Preconfiguring packages ...
Selecting previously unselected package libcommon-sense-perl:amd64.
(Reading database ... 116040 files and directories currently installed.)
Preparing to unpack .../00-libcommon-sense-perl_3.75-2build1_amd64.deb ...
Unpacking libcommon-sense-perl:amd64 (3.75-2build1) ...
Selecting previously unselected package libjson-perl.
Preparing to unpack .../01-libjson-perl_4.04000-1_all.deb ...
Unpacking libjson-perl (4.04000-1) ...
Selecting previously unselected package libtypes-serialiser-perl.
Preparing to unpack .../02-libtypes-serialiser-perl_1.01-1_all.deb ...
Unpacking libtypes-serialiser-perl (1.01-1) ...
Selecting previously unselected package libjson-xs-perl.
Preparing to unpack .../03-libjson-xs-perl_4.030-1build3_amd64.deb ...
Unpacking libjson-xs-perl (4.030-1build3) ...
Selecting previously unselected package libllvm14:amd64.
Preparing to unpack .../04-libllvm14_1%3a14.0.0-1ubuntu1.1_amd64.deb ...
Unpacking libllvm14:amd64 (1:14.0.0-1ubuntu1.1) ...
Selecting previously unselected package libpq5:amd64.
Preparing to unpack .../05-libpq5_14.11-0ubuntu0.22.04.1_amd64.deb ...
Unpacking libpq5:amd64 (14.11-0ubuntu0.22.04.1) ...
Selecting previously unselected package libpq-dev.
Preparing to unpack .../06-libpq-dev_14.11-0ubuntu0.22.04.1_amd64.deb ...
Unpacking libpq-dev (14.11-0ubuntu0.22.04.1) ...
Selecting previously unselected package postgresql-client-common.
Preparing to unpack .../07-postgresql-client-common_238_all.deb ...
Unpacking postgresql-client-common (238) ...
Selecting previously unselected package postgresql-client-14.
Preparing to unpack .../08-postgresql-client-14_14.11-0ubuntu0.22.04.1_amd64.deb
 ...
Unpacking postgresql-client-14 (14.11-0ubuntu0.22.04.1) ...
Selecting previously unselected package ssl-cert.
Preparing to unpack .../09-ssl-cert_1.1.2_all.deb ...
Unpacking ssl-cert (1.1.2) ...
Selecting previously unselected package postgresql-common.
Preparing to unpack .../10-postgresql-common_238_all.deb ...
Adding 'diversion of /usr/bin/pg_config to /usr/bin/pg_config.libpq-dev by postg
resql-common'
Unpacking postgresql-common (238) ...
Selecting previously unselected package postgresql-14.
Preparing to unpack .../11-postgresql-14_14.11-0ubuntu0.22.04.1_amd64.deb ...
Unpacking postgresql-14 (14.11-0ubuntu0.22.04.1) ...
Selecting previously unselected package postgresql.
Preparing to unpack .../12-postgresql_14+238_all.deb ...
Unpacking postgresql (14+238) ...
Setting up postgresql-client-common (238) ...
Setting up libpq5:amd64 (14.11-0ubuntu0.22.04.1) ...
Setting up libcommon-sense-perl:amd64 (3.75-2build1) ...
Setting up libpq-dev (14.11-0ubuntu0.22.04.1) ...
Setting up postgresql-client-14 (14.11-0ubuntu0.22.04.1) ...
update-alternatives: using /usr/share/postgresql/14/man/man1/psql.1.gz to provid
e /usr/share/man/man1/psql.1.gz (psql.1.gz) in auto mode
Setting up ssl-cert (1.1.2) ...
Could not create certificate. Openssl output was:
Error checking x509 extension section v3_req
4037CAD3117F0000:error:1100006C:X509 V3 routines:X509V3_parse_list:invalid empty
 name:../crypto/x509/v3_utl.c:383:
4037CAD3117F0000:error:11000069:X509 V3 routines:do_ext_nconf:invalid extension 
string:../crypto/x509/v3_conf.c:102:name=subjectAltName,section=DNS:localhost,
4037CAD3117F0000:error:11000080:X509 V3 routines:X509V3_EXT_nconf_int:error in e
xtension:../crypto/x509/v3_conf.c:48:section=v3_req, name=subjectAltName, value=
DNS:localhost,
dpkg: error processing package ssl-cert (--configure):
 installed ssl-cert package post-installation script subprocess returned error e
xit status 1
dpkg: dependency problems prevent configuration of postgresql-common:
 postgresql-common depends on ssl-cert (>= 1.0.11); however:
  Package ssl-cert is not configured yet.

dpkg: error processing package postgresql-common (--configure):
 dependency problems - leaving unconfigured
Setting up libllvm14:amd64 (1:14.0.0-1ubuntu1.1) ...
Setting up libtypes-serialiser-perl (1.01-1) ...
No apport report written because the error message indicates its a followup erro
r from a previous failure.
                          Setting up libjson-perl (4.04000-1) ...
dpkg: dependency problems prevent configuration of postgresql-14:
 postgresql-14 depends on postgresql-common (>= 229~); however:
  Package postgresql-common is not configured yet.
 postgresql-14 depends on ssl-cert; however:
  Package ssl-cert is not configured yet.

dpkg: error processing package postgresql-14 (--configure):
 dependency problems - leaving unconfigured
Setting up libjson-xs-perl (4.030-1build3) ...
No apport report written because the error message indicates its a followup erro
r from a previous failure.
                          dpkg: dependency problems prevent configuration of pos
tgresql:
 postgresql depends on postgresql-14; however:
  Package postgresql-14 is not configured yet.

dpkg: error processing package postgresql (--configure):
 dependency problems - leaving unconfigured
Processing triggers for man-db (2.10.2-1) ...No apport report written because Ma
xReports is reached already

Processing triggers for libc-bin (2.35-0ubuntu3.7) ...
Errors were encountered while processing:
 ssl-cert
 postgresql-common
 postgresql-14
 postgresql
needrestart is being skipped since dpkg has failed
E: Sub-process /usr/bin/dpkg returned an error code (1)
```
Ouch!  This did not go as planned.

Toby was able to fix this problem by running:
```
$ sudo rm /var/lib/dpkg/info/ssl-cert*
$ sudo dpkg --configure -D 777 ssl-cert
$ sudo apt -f install
```

Since I generally prefer running [debian](https://www.debian.org/), I decided
to switch to a debian 12 (bookworm) vm and start over.  I was able to get
through the postgres install without incident (postgres did not depend on
ssl-cert), so I'll proceed with the installation on debian, and circle back
later to figure out what to do about ssl (I have a feeling we're going to need
that).


## Step 3: Installing [Node.js](https://nodejs.org/en)
```
$ sudo mkdir -p /etc/apt/keyrings
$ curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
$ echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
```
Here I got back:
```
deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main
```
and then continued:
```
$ sudo apt-get update && sudo apt-get install -y nodejs
$ curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
$ echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
$ sudo apt-get update && sudo apt-get install -y yarn
```

4. Installing [Decidim](https://decidim.org/)
```
$ sudo apt install -y libicu-dev imagemagick
$ gem install decidim
$ decidim DecidimNOVAWebDev
```
That last step took several minutes and did a lot. I won't copy it all here.


5. Configuring the database
```
$ git clone https://github.com/rbenv/rbenv-vars.git "$(rbenv root)"/plugins/rbenv-vars
```
Then edit ``.rbenv-vars`` in my home directory, putting the following in it:
```
DATABASE_HOST=localhost
DATABASE_USERNAME=DecidimNOVAWebDev
DATABASE_PASSWORD=thepassword
```
I then tried to run:
```
$ rails db:create db:migrate
```
from inside the ``DecidimNOVAWebDev`` directory, but there was no
``DecidimNOVAWebDev`` database user with permissions to create a database.
So, I became the ``postgres`` user and ran:
```
postgres=# CREATE USER DecidimNOVAWebDev WITH PASSWORD 'thepassword';
```
from ``psql``. After which I again tried to run ``rails db:create db:migrate``
and... *drats*! I got this:
```
connection to server at "::1", port 5432 failed: FATAL:  password authentication failed for user "DecidimNOVAWebDev"
connection to server at "::1", port 5432 failed: FATAL:  password authentication failed for user "DecidimNOVAWebDev"
Couldn't create 'DecidimNOVAWebDev_development' database. Please check your configuration.
rails aborted!
ActiveRecord::ConnectionNotEstablished: connection to server at "::1", port 5432 failed: FATAL:  password authentication failed for user "DecidimNOVAWebDev"
connection to server at "::1", port 5432 failed: FATAL:  password authentication failed for user "DecidimNOVAWebDev"
/home/jelkner/DecidimNOVAWebDev/bin/rails:5:in `<top (required)>'
/home/jelkner/DecidimNOVAWebDev/bin/spring:10:in `block in <top (required)>'
/home/jelkner/DecidimNOVAWebDev/bin/spring:7:in `<top (required)>'

Caused by:
PG::ConnectionBad: connection to server at "::1", port 5432 failed: FATAL:  password authentication failed for user "DecidimNOVAWebDev"
connection to server at "::1", port 5432 failed: FATAL:  password authentication failed for user "DecidimNOVAWebDev"
/home/jelkner/DecidimNOVAWebDev/bin/rails:5:in `<top (required)>'
/home/jelkner/DecidimNOVAWebDev/bin/spring:10:in `block in <top (required)>'
/home/jelkner/DecidimNOVAWebDev/bin/spring:7:in `<top (required)>'
Tasks: TOP => db:create
(See full trace by running task with --trace)
```
