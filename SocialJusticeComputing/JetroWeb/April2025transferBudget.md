# Social Justice Computing April 2025 Wire Transfer to Jetro Web

| Item       | Amount    | Description                                       |
|------------|-----------|---------------------------------------------------|
| Stipends   | $750.00   | Shallon, Gabriel, Mulbah, Spencer, Daniel, Janet  |
| Salary     | $800.00   | Thomas and Freena                                 |  
| Savings    | $300.00   | Toward Jetro Web Office                           |
| **TOTAL**  | $1850.00  |                                                   | 


