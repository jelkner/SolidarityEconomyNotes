# Finding an Office for Jetro Web

Let's learn about our proposed location.

On Mr. Zawolo's original recommendation, we are proposing to find a place near
the new [University of Liberia campus](https://ul.edu.lr/new/).

I jumped on this suggestion, since it could be part of a long term plan to
develop an IT solidarity economy hub in the country involving Jetro Web, the
University of Liberia, the school we hope to build nearby, and other
initiatives that may be stimulated by all this activity in one area.

The name of campus is the Fendall Campus, and it is located
[here](https://www.google.com/maps/place/University+of+Liberia+-+Fendell+Campus./@6.376813,-10.618935,16z/data=!4m6!3m5!1s0xfa755f8ac24f56f:0x5e8da5815084837a!8m2!3d6.3768129!4d-10.6189349!16s%2Fg%2F1tdxb1gb?hl=en-US&amp;entry=ttu&amp;g_ep=EgoyMDI1MDMwNC4wIKXMDSoJLDEwMjExNDUzSAFQAw%3D%3D).

Let's start with an investigation of the Fendall community.

1. Where is it located? How proximate to the capital, Monrovia?
2. What office space is available there? How much does it cost?
3. What's in the neighborhood?
