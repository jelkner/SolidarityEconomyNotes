# Notes on Design Justice by Sasha Costanza-Chock

## Where to begin?

This book provides a sweeping introduction to a crucial topic, how to
strive for justice in the process by which we design the world we live in.

There are so many references to so many valuable resources, I want to first
try to wrap my head around the core of the issue, so I'll begin by extracting
quotes that make the big points, and leave the details for later.

## Sandra Braman's Introduction

- [I]n a democracy, every citizen should have the opportunity to take part
  in decision making about what we do, how we operate, how we structure the
  world in which we live. (p. xiii)
- Now, though, we recognize that the world for which we are making decisions is
  not just social. It is sociotechnical, and in the digital environment, as the
  saw says, “code is law,” responsible for providing an infrastructure of
  constraints and affordances that affect what we do, how we operate, and how we
  structure the social world within which we live. (p. xiii)

## Preface

- This book is about the relationship between design and power. (p. xviii)
- In essence, it’s a call for us to heed the growing critiques of the ways that
  design (of images, objects, software, algorithms, sociotechnical systems,
  the built environment, indeed, everything we make) too often contributes to
  the reproduction of systemic oppression. (p. xviii)

## Introduction

- In his classic text *Design for the Real World*, Victor Papanek positions
  design as a universal practice in human communities: "All [people]
  are designers. ... Design is the conscious effort to impose a meaningful
  order." (p. 13)
- Closely linked to intersectionality, but less widely used today, the
  *matrix of domination* is a term developed by Black feminist scholar,
  sociologist, and past president of the American Sociological Association
  Patricia Hill Collins to refer to race, class, and gender as interlocking
  systems of oppression. It is a conceptual model that helps us think about how
  power, oppression, resistance, privilege, penalties, benefits, and harms are
  systematically distributed. (p. 20)

## Chapter 1: Design Values: Hard-Coding Liberation?

- Technology is always a form of *social* knowledge, practices and products. It
  is the result of conflicts and compromises, the outcomes of which depend
  primarily on the distribution of power and resources between different groups
  in society. -- Judy Wajcman, *Feminism Confronts Technology* (p. 32)
- Yet Facebook in general, and Facebook Events specifically, provides terrible
  tools for the most important task of community organizers: to move people up
  the ladder of engagement. (p. 34)
- Why do the most popular social media platforms provide such limited
  affordances for the important work of community organizing and movement
  building? Why is the time, energy, and brilliance of so many designers,
  software developers, product managers, and others who work on platforms
  focused on optimizing our digital world to capture and monetize our
  attention, over other potential goals (e.g., maximizing civic engagement,
  making environmentally sustainable choices, building empathy, or achieving
  any one of near-infinite alternate desirable outcomes)? (p. 35)
- Put another way, why do we continue to design technologies that reproduce
  existing systems of power inequality when it is so clear to so many that we
  urgently need to dismantle those systems? (p. 36)
- What will it take for us to transform the ways that we design
  technologies (sociotechnical systems) of all kinds, including digital
  interfaces, applications, platforms, algorithms, hardware, and
  infrastructure, to help us advance toward liberation? (p. 36)

## Como Dijo el Che 

«El revolucionario verdadero está guiado por grandes sentimientos de amor»

## Chapter 2: Design Practices: "Nothing about Us without Us"

- If you have come here to help me, you are wasting your time. But if you have
  come because your liberation is bound up with mine, then let us work
  together.  --­Lilla Watson, Australian Aboriginal activist and artist (p. 70)
- Intersectional inequality systematically structures paid professional design
  work. Professional design jobs in nearly all fields are disproportionately
  allocated to people who occupy highly privileged locations within the matrix
  of domination. (p. 73)
- Power shapes participation in all design processes, including in
  [Participatory Design], and the politics of participation are always
  intersectionally classed, gendered, and raced. (p. 88)


