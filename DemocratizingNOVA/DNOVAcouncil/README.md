# [DNOVA Council](https://nextsystem.gmu.edu/projects/dnova/council)

## The DNOVA Council (Jan 2024-Dec 2025 Term) 

* Amy Best, Director, Director of the Center for Social Science Research, George
Mason University
* Karla Bruce, Chief Equity Office, Office of the County Executive, Fairfax County
* Virginia Diamond, President, Northern Virginia Labor Federation AFL-CIO
* Jeff Elkner, NOVA Web Development
* Ivana Escobar, Director of Collective Impact, United Community
* Johanna Garay, Organizer, SECOSOL 
* My Lan Tran, President, Virginia Asian Chamber of Commerce
* Ben Manski, Director, Next System Studies, George Mason University
* Benton Murphy, Director of Fund Administration & Special Projects, Greater Washington Community Foundation
* Pallas Washington, Deputy Director, Neighborhood and Community Services, Fairfax County


## Ex-Officio Members include:

* Ronnie Galvin (Greater Washington Community Foundation
* Sarah McKinley (The Democracy Collaborative)
* Sara Aftab, Dhruv Deepak, Eleanor Finley, John Dale, and Kellie Wilkerson (all of George Mason University). 
