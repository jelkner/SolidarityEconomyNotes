# Mujeres Manos a la Obra Virginia Cleaning

*Note*: Emailed Matt Fienstein (matt@usworker.coop) on 2022-10-07 about the
[Co-op Clinic](https://www.usworker.coop/clinic/) and the possibility of
hiring a consultant to help with business planning.

Referred to Matt by USFWC Membership Manager Kate Barut in a phone call with
her that day.
