# Cooperativas y Socialismo: Una mirada desde Cuba

## Preguntas

1. Cuba inició nuevos experimentos con cooperativas de trabajadores hace casi 
   una década. ¿Puede decirnos qué motivó estos experimentos y actualizarnos
   sobre cómo van? 
1. ¿Cómo ve en Cuba el papel de las cooperativas de trabajo en relación con la
   construcción de una sociedad socialista? 
1. ¿Qué contradicciones crea el modelo cooperativo de trabajo en relación con
   el proyecto socialista? 
1. ¿Hay características particulares de las cooperativas de trabajo en el
   contexto de una sociedad socialista como la cubana que las diferencien de
   las cooperativas existentes bajo el capitialismo? 
1. El movimiento cooperativo de trabajadores ha estado creciendo en los
   Estados Unidos en los últimos años, aunque estamos mucho menos unidos aquí
   en el objetivo de terminar con la explotación capitalista a través de la
   propiedad democrática. ¿Qué lecciones puede compartir para los socialistas
   en los Estados Unidos que se puedan aprender de la experiencia de Cuba con
   las cooperativas de trabajadores?  
1. ¿Están las cooperativas de trabajadores en Cuba proporcionando una
   alternativa en los tiempos económicos difíciles actuales a la propiedad
   privada asociada con la esclavitud asalariada?  
1. ¿Cree que existen oportunidades para compartir las mejores prácticas y la
   solidaridad de trabajador a trabajador entre nuestros dos países?  


## Questions

1. Cuba began new experiments with worker cooperatives almost a decade ago. Can
   you tell us what motivated these experiments and update us on how they are
   going?
1. How in Cuba do you see the role of worker cooperatives as they relate to
   the building of a socialist society?
1. What contradictions does the worker cooperative model create in relation to
   the socialist project?
1. Are there particular features of worker cooperatives in the context of a
   socialist society like Cuba's that make them different from cooperatives
   existing under capitialism?
1. The worker cooperative movement has been growing in the United States in
   recent years, though we are much less united here on the goal of ending
   capitalist exploitation through democratic ownership. What lessons can you
   share for socialists in the United States that can be learned from Cuba's
   experience with worker cooperatives?
1. Are worker cooperatives in Cuba providing an alternative under the present
   challenging economic times to private ownership associated with wage
   slavery?
1. Do you think there are opportunities for shared best practices and worker to
   worker solidarity between our two countries?


## Camila Piñeiro Harnecker

Camila Piñeiro Harnecker ha sido, por más de 15 años, investigadora, profesora
y consultora en economía social y solidaria, especializada en cooperativismo y
políticas públicas hacia el sector. Su trabajo se ha centrado en Cuba,
Venezuela y Ecuador, con énfasis en la gobernanza y administración, y en
particular en las cooperativas de trabajadores y productores. Es profesora del
Master en Gestión y Desarrollo de Cooperativas de FLACSO en la Universidad de
La Habana, y autora de cuatro libros y más de 50 capítulos de libros y
artículos sobre estos temas
(https://www.researchgate.net/profile/Camila-Pineiro-Harnecker
y https://rebelion.org/autor/camila-pineiro-harnecker/).

Camila Piñeiro Harnecker has been a researcher, professor and consultant in the
social and solidarity economy for more than 15 years, specializing in
cooperativism and public policies towards that sector.  Her work has focused on
Cuba, Venezuela and Ecuador, with an emphasis on governance and administration,
and in particular in workers' and producers' cooperatives. She is a professor
of the Master in Management and Development of Cooperatives program of FLACSO
at the University of Havana, and author of four books and more than 50 book
chapters and articles on these topics
(https://www.researchgate.net/profile/Camila-Pineiro-Harnecker
and https://rebelion.org/autor/camila-pineiro-harnecker/).


## Sonia Umanzor

Sonia Umanzor, activista de derechos humanos desde hace mucho tiempo, llegó a
Washington, DC a principios de la década de 1980 como refugiado de la guerra en
El Salvador. Ella tiene continuó siendo una activista comunitaria en su nuevo
hogar, donde ayudó a fundar la Clínica del Pueblo en 1983 para atender al gran
número de refugiados centroamericanos y se ha mantenido firme en la lucha por
los derechos de los inmigrantes. Ella es miembro del Foro de São Paulo,
Washington, fundador de Casa Rutilio Grande y miembro del partido político FMLN
en El Salvador. 

Sonia Umanzor, a long time human rights activist, arrived in the Washington,
DC area in the early 1980s as a refugee from the war in El Salvador. She has
continued to be a community activist in her new home, where she helped found
the Clínica del Pueblo in 1983 to serve the large Central American refugee
community and has remained firm in the struggle for immigrants’ rights. She is
a member of the Forum of São Paulo, Washington, founder of Casa Rutilio Grande
and a member of the FMLN political party in El Salvador.


## Arturo Griffiths

Arturo Griffiths ha sido un activista comunitario, laboral y de derechos
laborales desde emigrando a Washington, D.C. desde Panamá en la década de 1970.
Su larga lista de actividades incluyen ayudar a fundar el Centro Juvenil
Latinoamericano y el D.C.  Coalición por la inmigración y el pleno empleo,
organizando movilizaciones de empleo para la Juventud, el Festival
Hispanoamericano y el Festival Multicultural de la Ciudad de 1992 Cumbre de
liderazgo. Como organizador sindical y laboral, Arturo ha trabajado para ¡ÚNETE
AQUÍ! Local 25, SEIU Local 500 y DC Jobs with Justice. En 2014 él ayudó a
organizar y ahora dirige Trabajadores Unidos de Washington DC, un día sin fines
de lucro comunitaria de trabajadores, que trabaja en la construcción de
cooperativas para los trabajadores inmigrantes de DC. 

Arturo Griffiths has been a community, labor and worker rights activist since
immigrating to Washington, D.C. from Panama in the 1970s. His long list of
activities include helping found the Latin American Youth Center and the D.C.
Coalition on Immigration and Full Employment, organizing mobilizations of Jobs
for Youth, the Hispanic American Festival, and the 1992 Citywide Multicultural
Leadership Summit. As a union and labor organizer, Arturo has worked for
UNITEHERE! Local 25, SEIU Local 500, and DC Jobs with Justice. In 2014 he
helped organize and now leads Trabajadores Unidos de Washington DC, a day
laborer community-based non-profit, which is working on building cooperatives
for DC immigrant workers.


## Anuncio

Este evento es una continuación de un evento similar realizado el año pasado
sobre las cooperativas de trabajadores y el movimiento por el socialismo con
Harnecker junto con Jessica Gordon-Nembhard y Richard Wolff. 

*Bio de Camila aqui*

Sonia Umanzor y Arturo Griffiths son activistas de Washington DC desde hace
mucho tiempo que entrevistarán a Camila y liderarán una discusión sobre lo que 
los socialistas en el área de DC podemos aprender de la experiencia actual con
las cooperativas en Cuba.
