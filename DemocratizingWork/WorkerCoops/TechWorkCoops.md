# Notes on Efforts to Create a Federation of US Tech Worker Coops

## Links

* [Tech coops list](https://github.com/hng/tech-coops)
* [FACT\[TIC\]](https://facttic.org.ar/)
* [CoTech](https://www.coops.tech/)
* [https://techworker.coop/](https://techworker.coop/)
