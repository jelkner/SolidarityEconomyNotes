## Employee Ownership

* [Huawei Is Going to Beat Trump with Human Resources, Not Technology (Pt 1 of 3)](https://jefftowson.com/2019/10/huawei-is-going-to-beat-trump-with-human-resources-not-technology-pt-1-of-3/)

## The Socialist Entrepreneur 

* [Cooperative nuts and bolts: minimum profit plow-back rules](https://thesocialistentrepreneur.com/2018/08/15/cooperative-nuts-and-bolts-minimum-profit-plow-back-rules/)
