# Worker Cooperatives and the Movement for Socialism

Notes and resources concerning worker cooperatives as they relate to the
movement for socialism.

## Resources

  * [Webinar: Worker Cooperatives and the Movement for Socialism](https://www.youtube.com/watch?v=1gf5j_aNmKE&feature=youtu.be)
  * [Quotes from Worker Cooperatives and the Movement for Socialism](https://codeberg.org/jelkner/WorkerCooperatives/src/branch/main/cjs_1dc_worker_coop_webinar_notes.md)
