﻿# Meeting Notes June 28, 2021

EtherPad with questions for Yael to address at show and tell:

[https://pad.drutopia.org/p/private\_facttic-show-and-tell](https://pad.drutopia.org/p/private\_facttic-show-and-tell)

## Attendees:
  * Nicolas -- [Fiqus](https://fiqus.coop/en/)
  * Yael -- [Devecoop](https://devecoop.com/)
  * Keegan -- [Agaric](https://agaric.coop/)
  * Jeff -- [NOVA Web Development](https://novawebdevelopment.org/)
  * Nurul -- [Chi Commons Cooperative](https://www.chicommons.coop/)
  * Colombene -- [NOVA Web Development](https://novawebdevelopment.org/)
  * Micky -- [Agaric](https://agaric.coop/)
  * Steve -- [Chi Commons Cooperative](https://www.chicommons.coop/)
    
## Global CoOp Network
  * ARG - [FACT\[TIC\]](https://facttic.org.ar/)
  * UK - [CoTech](https://www.coops.tech/)

To join (if you are a member of a tech worker coop) click
[here](https://patio.ica.coop/chat/signup\_user\_complete/?id=9utkpziioby4tbas4tzegzzzeo).

## Argentina:
  * [Inter-Cooperative Flow of Work](https://facttic.org.ar/fit/)

**Goal** : We are onboard with building a mature coop network and are looking
to build our own thing in the US, and join the global network.

What happened in CoTech:

The network is open to anyone participating in job opportunities, which allows
for collaboration with freelancers in the same level as other coops. Many
developers in cotech, then, are members of the network, but do not stay in the
coops (since they get higher pay elsewhere with less co-governing
responsibilities)

In FACT\[TIC\] network members are all cooperatives.

## Links to previous talk that we held at an Agaric Show-and-Tell about FACT\[TIC\] and FIT: 
  * https://fiqus.github.io/FIT-talk-en/
  * https://fiqus.github.io/FIT-talk-2-en/
  * https://agaric.coop/blog/show-and-tell-agaric-sharing-work-other-coops
    
## Simple landing with info related to our trip to UK: https://uktrip2019.fiqus.coop/
    
## Possible Topics of the July 29 Show and Tell

Yael is open to input via email over the next weeks:
  1. Origin story
  1. Curent state in words/ pictures / numbers (member coops, no. workers,
     revenues, no. clients, etc)
  1. Benefits of cooperation
  1. How info flow and referrals work
  1. How mentoring / peer coaching works
  1. Tools (social / human and technical)
  1. [CoopHub.io](https://coophub.io/) as alternative to Github?
  1. Lessons learned
  1. Best practices working with i. freelancers ii. government iii. other coops

[EventoL](https://eventol.org/) is an example of the kind of platform
cooperativism that our movement can build.
