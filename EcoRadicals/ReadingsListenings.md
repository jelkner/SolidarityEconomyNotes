# Readings by Week

## Week 2 (January 29, 2025)

* [Hope in Common](https://davidgraeber.org/wp-content/uploads/2008-Hope-in-common.pdf) by David Greber
* [Worker Cooperatives Part 1](https://www.upstreampodcast.org/workercoops1) (Podcast)


### Recommended

* [Worker Co-Ops Have a Role to Play in Socialist Strategy](https://jacobin.com/2024/05/cooperatives-dsa-left-strategy-solidarity) by Daniel Wortel-London
* [ICA Cooperative identity, values &amp; principles](https://ica.coop/en/cooperatives/cooperative-identity/)
* [A New View of Society, Or, Essays on the Principle of the Formation of the
  Human Character, and the Application of the Principle to Practice](https://la.utexas.edu/users/hcleaver/368/368OwenNewViewtable.pdf)
  by Robert Owen


### Websites to Visit

* [Mondragon](https://www.mondragon-corporation.com/en/)
* [Cooperation Jackson](https://cooperationjackson.org/) 
* [Arizmendi Bakery](https://arizmendibakery.com/)
* [Namma Yatri](https://nammayatri.in/)
* [SEWA Cooperative Federation](https://www.sewafederation.org/)
* [New Era Windows Cooperative](https://newerawindows.com)
