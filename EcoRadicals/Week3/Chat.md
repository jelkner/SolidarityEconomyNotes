# Chat Links and Comments

* read.ai meeting notes to Everyone RM Ines and JM added read.ai meeting notes
  to the meeting. Read provides AI generated meeting summaries to make meetings
  more effective and efficient. View our [Privacy
  Policy](https://www.read.ai/pp)

* [Notes Eco Radicals SPR 2025](https://docs.google.com/document/d/1pXxpAnodWOlO2LmHjsoXbbHDArmFQmQiDyXEQ27ftJc/edit?usp=drive_open&ouid=101921177445088814492)

* [Overview Feb 5](https://docs.google.com/document/d/10aaw1TeuBGB4fMxO_Zr6ijGtVVMV0EKjB3Oqv8d88aU/edit?usp=sharing)

* **JE** How do you assure workers recognize the broader interests of society
  and not just their own immediate interests?

* **Marie Therese Kane** In my experience, “open membership” has meant open to
  anyone who meets membership criteria (that outlines community guidelines, etc)

* **Trebor Scholz, PCC/The New School**: [Tulsa race massacre](https://en.wikipedia.org/wiki/Tulsa_race_massacre),
  [Emmett Till](https://en.wikipedia.org/wiki/Emmett_Till),
  [Lucia di Lammermoor](https://en.wikipedia.org/wiki/Lucia_di_Lammermoor?utm_source=chatgpt.com).
  [Obran Cooperative](https://www.obran.coop/), established in 2019 by a group
  of returning citizens in Baltimore City, is a worker-owned cooperative
  conglomerate. Finally, look at
  [Platform Co-op Resource Library](https://resources.platform.coop/).
