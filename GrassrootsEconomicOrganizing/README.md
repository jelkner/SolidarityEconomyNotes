# Grassroots Economic Organizing

## Links

* [Cooperative Project Management with Louder than
Ten](https://www.youtube.com/watch?v=G8lNBSZk_x0)
* [Cooperative Enterprise and Market
Economy](https://geo.coop/articles/cooperative-enterprise-and-market-economy)
