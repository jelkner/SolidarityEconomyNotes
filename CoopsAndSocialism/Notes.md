# Notes and Resources on the Relationship Between Worker Coops and Socialism

## Resources Supporting Coops in Struggle for Socialism 

* [Build Socialist Enterprises](https://www.dsausa.org/democratic-left/build-socialist-enterprises/) by Alexander Kolokotronis
* [Socialism and Worker's Coops](https://www.counterpunch.org/2019/05/10/socialism-and-workers-coops/) by Richard Wolff
* [Cooperatives On the Path to Socialism?](https://monthlyreview.org/2015/02/01/cooperatives-on-the-path-to-socialism/) by Peter Marcuse
* [Cooperatives in Socialist Construction: Commoners and Cooperators Key to Cuba's 21st Century Socialism](https://geo.coop/story/cooperatives-socialist-construction) by Cliff DuRand

## Resources Opposing Coops in Struggle for Socialism 

* [You Can't Win without a Fight: Why Worker Cooperatives Are a Bad Strategy](https://organizing.work/2021/01/you-cant-win-without-a-fight-why-worker-cooperatives-are-a-bad-strategy/) by Carmen Molinari

## Minsun Ji and a Dialectical Approach to the Question

* [With or without class: Resolving Marx's Janus-faced interpretation of worker-owned cooperatives](https://www.academia.edu/39609495/With_or_without_class_Resolving_Marxs_Janus_faced_interpretation_of_worker_owned_cooperatives)
* [Platform Cooperatives with Minsun Ji](https://breadtube.tv/democracyatwork/all-things-co-op-platform-cooperatives-with-minsun-ji/)
